package com.mathworks.internal.business.product.archivefile.ws.controller;

import com.mathworks.internal.business.product.archivefile.api.entity.ArchiveFile;
import com.mathworks.internal.business.product.archivefile.api.service.ArchiveFileService;


import com.mathworks.internal.foundation.rest.pagesupport.HeaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping({"/v1/archiveFiles", "/v1/archiveFiles/"})
public class ArchiveFileController {

  @Autowired
  private ArchiveFileService archiveFileService;
  @Autowired
  private HeaderHelper headerHelper;

  @GetMapping
  public List<ArchiveFile> getArchiveFilesByFilter(@RequestParam Map<String, String> params, HttpServletResponse response) {
    List<ArchiveFile> archiveFiles = archiveFileService.findArchiveFilesByFilter(params);
    headerHelper.addPageCounts(response, archiveFiles);
    return archiveFiles;
  }
}
