package com.mathworks.internal.business.product.archivefile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.mathworks.internal.core.logging.support.LogTrackingConfig;
import com.mathworks.internal.core.logging.support.logback.LogControlConfig;
import com.mathworks.internal.core.healthmonitorapp.HealthConfigurations;
import com.mathworks.internal.core.cache.CacheAdminConfiguration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.context.annotation.Import;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.jdbc.DataSourcePoolMetricsAutoConfiguration;


@EnableJpaRepositories
@EnableTransactionManagement
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, DataSourcePoolMetricsAutoConfiguration.class})
@Import({LogTrackingConfig.class,LogControlConfig.class, HealthConfigurations.class, CacheAdminConfiguration.class})
public class ArchivefilewsApplication {

  public static void main(String[] args) {
    SpringApplication.run(ArchivefilewsApplication.class, args);
  }
}
