package com.mathworks.internal.business.product.archivefile.api.service.support;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

/**
 * Extension of LinkedMultiValueMap<String, String>, providing a size(key) method to return the size of the value list
 * for the given key.  Intended for use dealing with multi-value parameter lists for REST services.
 */
public class MultiParams extends LinkedMultiValueMap<String, String> implements MultiValueMap<String, String> {

  /**
   * Returns the size of the list of values for the given key, or 0 if the key is not in the map.
   *
   * @param key String key
   * @return int size of the list of values
   */
  public int size(String key) {
    if (!containsKey(key)) {
      return 0;
    }
    return get(key).size();
  }

  public Boolean getBoolean(String key) {
    List<String> values = Arrays.asList("true", "1", "y", "Y", "t", "T");
    if (!containsKey(key)) {
      return null;
    }
    return values.contains(getFirst(key));
  }

}
