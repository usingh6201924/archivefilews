package com.mathworks.internal.business.product.archivefile.api.service.support;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class Validator {

  /**
   * Validate the given list of params, and return a list of String error messages or an empty list if no validation errors
   *
   * @param params            to be validated
   * @param validParams       the valid list of params - if the map of params contains any keys not in this list, they are invalid
   * @return List<String> of validation errors or empty list if none
   */
  public List<String> validate(MultiParams params, List<String> validParams) {
    List<String> errors = new ArrayList<>();
    errors.addAll(checkInvalidParams(params, validParams));
    return errors;
  }


  private Collection<String> checkInvalidParams(MultiParams params, List<String> validParams) {
    if (!validParams.containsAll(params.keySet())) {
      return singletonList("Can only include these params: " + validParams);
    }
    return emptyList();
  }
}
