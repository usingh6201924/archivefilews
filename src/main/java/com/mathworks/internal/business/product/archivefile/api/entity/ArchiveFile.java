
package com.mathworks.internal.business.product.archivefile.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.*;

@Entity
@Table(name = "archive_file", schema = "dbo")
public class ArchiveFile {
  @JsonIgnore
  @Id
  private Integer id;

  @Column(name = "arch_file_typ_id")
  private Integer archiveFileTypeId;

  @Column(name = "arch_file_name")
  private String archiveFileName;

  @Column(name = "arch_file_byte_size", length = 100)
  private Long archiveFileByteSize;

  @Column(name = "arch_file_md5_checksum")
  private String archiveFileMD5Checksum;

  @Column(name = "arch_uncmprssd_file_byte_size")
  private Integer archiveUncompressedFileByteSize;

  @Column(name = "file_path_randm_extn")
  private String filePathRandomExtension;

  private String platform;

  @Column(name = "release_name")
  private String releaseName;

  @Column(name = "customer_release_string")
  private String customerReleaseString;

  @Column(name = "base_code")
  private String baseCode;

  private String version;

  @Column(name = "version_name")
  private String versionName;

  @Column(name = "platform_type")
  private String platformType;

  @Column(name = "release_availability_date")
  private String releaseAvailabilityDate;

  @Column(name = "license_file_feature_name")
  private String licenseFileFeatureName;

  @Column(name = "license_file_feature_id")
  private Integer licenseFileFeatureId;

  @Column(name = "arch_file_sha256_checksum")
  private String archiveFileSHA256Checksum;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getArchiveFileTypeId() {
    return archiveFileTypeId;
  }

  public void setArchiveFileTypeId(Integer archiveFileTypeId) {
    this.archiveFileTypeId = archiveFileTypeId;
  }

  public String getArchiveFileName() {
    return archiveFileName;
  }

  public void setArchiveFileName(String archiveFileName) {
    this.archiveFileName = archiveFileName;
  }

  public Long getArchiveFileByteSize() {
    return archiveFileByteSize;
  }

  public void setArchiveFileByteSize(Long archiveFileByteSize) {
    this.archiveFileByteSize = archiveFileByteSize;
  }

  public String getArchiveFileMD5Checksum() {
    return archiveFileMD5Checksum;
  }

  public void setArchiveFileMD5Checksum(String archiveFileMD5Checksum) {
    this.archiveFileMD5Checksum = archiveFileMD5Checksum;
  }

  public Integer getArchiveUncompressedFileByteSize() {
    return archiveUncompressedFileByteSize;
  }

  public void setArchiveUncompressedFileByteSize(Integer archiveUncompressedFileByteSize) {
    this.archiveUncompressedFileByteSize = archiveUncompressedFileByteSize;
  }

  public String getFilePathRandomExtension() {
    return filePathRandomExtension;
  }

  public void setFilePathRandomExtension(String filePathRandomExtension) {
    this.filePathRandomExtension = filePathRandomExtension;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public String getReleaseName() {
    return releaseName;
  }

  public void setReleaseName(String releaseName) {
    this.releaseName = releaseName;
  }

  public String getCustomerReleaseString() {
    return customerReleaseString;
  }

  public void setCustomerReleaseString(String customerReleaseString) {
    this.customerReleaseString = customerReleaseString;
  }
  public String getBaseCode() {
    return baseCode;
  }

  public void setBaseCode(String baseCode) {
    this.baseCode = baseCode;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getVersionName() {
    return versionName;
  }

  public void setVersionName(String versionName) {
    this.versionName = versionName;
  }

  public String getPlatformType() {
    return platformType;
  }

  public void setPlatformType(String platformType) {
    this.platformType = platformType;
  }

  public String getReleaseAvailabilityDate() {
    return releaseAvailabilityDate;
  }

  public void setReleaseAvailabilityDate(String releaseAvailabilityDate) {
    this.releaseAvailabilityDate = releaseAvailabilityDate;
  }

  public String getLicenseFileFeatureName() {
    return licenseFileFeatureName;
  }

  public void setLicenseFileFeatureName(String licenseFileFeatureName) {
    this.licenseFileFeatureName = licenseFileFeatureName;
  }

  public Integer getLicenseFileFeatureId() {
    return licenseFileFeatureId;
  }

  public void setLicenseFileFeatureId(Integer licenseFileFeatureId) {
    this.licenseFileFeatureId = licenseFileFeatureId;
  }

  public String getArchiveFileSHA256Checksum() {
    return archiveFileSHA256Checksum;
  }

  public void setArchiveFileSHA256Checksum(String archiveFileSHA256Checksum) {
    this.archiveFileSHA256Checksum = archiveFileSHA256Checksum;
  }

}
