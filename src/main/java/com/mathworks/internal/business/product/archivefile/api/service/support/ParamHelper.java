package com.mathworks.internal.business.product.archivefile.api.service.support;

import java.util.*;

public class ParamHelper {

  /**
   * Parses the given Map into a MultiParms object, which changes each map value into a List of String values
   *
   * @param params a Map<String, String> of parameters where a value may be a comma delimited list
   * @return a MultiParams object which has an underlying MultiValueMap so eah value is a List<String
   */
  public MultiParams parseParams(Map<String, String> params) {
    MultiParams parsedParams = new MultiParams();
    for (Map.Entry<String, String> entry : params.entrySet()) {
      String value = entry.getValue();
      List<String> values = Arrays.asList(value.split("\\s*,\\s*"));
      parsedParams.put(entry.getKey(), values);
    }
    return parsedParams;
  }
}
