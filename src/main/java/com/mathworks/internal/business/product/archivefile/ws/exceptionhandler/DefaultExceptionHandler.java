package com.mathworks.internal.business.product.archivefile.ws.exceptionhandler;

import com.mathworks.internal.foundation.rest.common.Message;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import com.mathworks.internal.foundation.rest.error.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;

@ControllerAdvice
public class DefaultExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourceNotFoundException.class, NoSuchElementException.class})
    @ResponseBody
    public Message handleMissingResourceElement(HttpServletRequest req, Exception ex) {
        return createMessage(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Message handleInternalServerError(HttpServletRequest req, Exception ex) {
        LOG.error("Internal server Error : ", ex);
        return createMessage(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public Message handleMissingParamError(HttpServletRequest req, BadRequestException ex) {
        return createMessage(ex.getMessage());
    }

    protected Message createMessage(String text) {
        Message message = new Message(text);
        message.setDetail(text);
        return message;
    }
}
