package com.mathworks.internal.business.product.archivefile.api.service;

import com.mathworks.internal.business.product.archivefile.api.entity.ArchiveFile;
import com.mathworks.internal.business.product.archivefile.api.repository.ArchiveFileRepository;
import com.mathworks.internal.business.product.archivefile.api.service.support.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mathworks.internal.foundation.rest.error.BadRequestException;

import java.util.*;

@Service
public class ArchiveFileService {
  @Autowired
  private ArchiveFileRepository archiveFileRepository;
  private Validator validator;
  private ParamHelper paramHelper;

  private static final String PLATFORMS = "platforms";
  private static final String RELEASE_NAMES = "releaseNames";
  private static final String CUSTOMER_RELEASE_STRINGS = "customerReleaseStrings";
  private static final String PLATFORM_TYPES = "platformTypes";
  private static final String BASE_CODES = "baseCodes";
  private static final List<String> PARAMS = Arrays.asList(PLATFORMS, RELEASE_NAMES, CUSTOMER_RELEASE_STRINGS, PLATFORM_TYPES, BASE_CODES);

  public ArchiveFileService() {
    validator = new Validator();
    paramHelper = new ParamHelper();
  }

  public List<ArchiveFile> findArchiveFilesByFilter(Map<String, String> params) {
    MultiParams parsedParams = parseParams(params);
    List<String> errors = validator.validate(parsedParams, PARAMS);
    if (!errors.isEmpty()) {
      throw new BadRequestException("Validation errors:" + errors);
    }
    return archiveFileRepository.findArchiveFilesByFilter(parsedParams.size(PLATFORMS),
        parsedParams.get(PLATFORMS), parsedParams.size(RELEASE_NAMES), parsedParams.get(RELEASE_NAMES),
        parsedParams.size(CUSTOMER_RELEASE_STRINGS), parsedParams.get(CUSTOMER_RELEASE_STRINGS),
        parsedParams.size(PLATFORM_TYPES), parsedParams.get(PLATFORM_TYPES), parsedParams.size(BASE_CODES),
        parsedParams.get(BASE_CODES));
  }

  private MultiParams parseParams(Map<String, String> params) {
    return paramHelper.parseParams(params);
  }

}
