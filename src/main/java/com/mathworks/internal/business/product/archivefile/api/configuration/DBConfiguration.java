package com.mathworks.internal.business.product.archivefile.api.configuration;

import com.mathworks.internal.foundation.namedresource.ResourceHelper;
import com.mathworks.internal.core.healthcheck.database.sqlserver.SqlServerDatabaseCheck;
import com.mathworks.internal.core.healthmonitor.AbstractHealthCheck;

import org.hibernate.dialect.SQLServer2012Dialect;
import org.springframework.context.annotation.*;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.orm.jpa.JpaTransactionManager;

import jakarta.persistence.EntityManagerFactory;

import javax.sql.DataSource;

import java.util.Properties;

@Configuration
public class DBConfiguration {

  private static final String DATASOURCE_NAMED = "productdataauthoraws";

  @Bean
  public DataSource dataSource() {
    ResourceHelper resourceHelper = new ResourceHelper();
    return resourceHelper.getJdbcDataSource(DATASOURCE_NAMED);
  }

  @Bean
  public AbstractHealthCheck databaseHealthCheck(){
    return new SqlServerDatabaseCheck(DATASOURCE_NAMED, dataSource());
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactoryBean.setDataSource(dataSource());
    entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    entityManagerFactoryBean.setPackagesToScan("com.mathworks.internal.business.product.archivefile.api.entity");
    Properties jpaProperties = new Properties();
    jpaProperties.put("hibernate.dialect", SQLServer2012Dialect.class.getName());
    jpaProperties.put("hibernate.show_sql","false");
    jpaProperties.put("hibernate.format_sql","false");
    entityManagerFactoryBean.setJpaProperties(jpaProperties);
    return entityManagerFactoryBean;
  }

  @Bean
  public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory);
    return transactionManager;
  }
}
