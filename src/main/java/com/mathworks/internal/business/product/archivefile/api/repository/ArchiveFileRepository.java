package com.mathworks.internal.business.product.archivefile.api.repository;

import com.mathworks.internal.business.product.archivefile.api.entity.ArchiveFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArchiveFileRepository extends JpaRepository<ArchiveFile, Integer> {
  @Query("SELECT a FROM ArchiveFile a "
      + "WHERE  "
      + " (?1 = 0 or a.platform IN (?2)) and "
      + " (?3  = 0 or a.releaseName IN (?4)) and "
      + " (?5  = 0 or a.customerReleaseString IN (?6)) and "
      + " (?7  = 0 or a.platformType IN (?8)) and "
      + " (?9  = 0 or a.baseCode IN (?10)) ")
  List<ArchiveFile> findArchiveFilesByFilter(int platformsSize, List<String> platforms, int releaseNamesSize, List<String> releaseNames,
                                             int customerReleaseStringsSize, List<String> customerReleaseStrings,
                                             int platformTypesSize, List<String> platformTypes,
                                             int baseCodesSize, List<String> baseCodes);

}