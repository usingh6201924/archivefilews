package com.mathworks.internal.business.product.archivefile.ws.configuration;

import com.mathworks.internal.core.springsecurity.filter.SpringSecurityFilter;
import com.mathworks.internal.core.rest.filter.RestLogFilter;
import com.mathworks.internal.core.healthcheck.auth.AuthenticationWsAliveCheck;
import com.mathworks.internal.core.healthmonitor.AbstractHealthCheck;
import com.mathworks.internal.foundation.namedresource.ResourceHelper;
import com.mathworks.internal.foundation.rest.pagesupport.HeaderHelper;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

  @Value("${spring.application.name}")
  private String applicationName;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**");
  }
  
  @Bean
  public SpringSecurityFilter springSecurityFilter() {
    SpringSecurityFilter springSecurityFilter = new SpringSecurityFilter();
    springSecurityFilter.setCallerIdRequired(true);
    springSecurityFilter.addToWhiteList("/swagger-ui");
    return springSecurityFilter;
  }

  @Bean
  public RestLogFilter logFilter() {
    RestLogFilter restLogFilter =  new RestLogFilter();
    restLogFilter.addExcludePattern("/admin/health");
    restLogFilter.addExcludePattern("/admin/metrics");
    return restLogFilter;
  }

  /**
   * Provides helper class from namedresource to simplify named resource lookups.
   * @return ResourceHelper helper class
   */
  @Bean
  public ResourceHelper resourceHelper() {
    return new ResourceHelper();
  }

  @Bean
  public AbstractHealthCheck authHealthCheck(){
    return new AuthenticationWsAliveCheck();
  }

  @Bean
  public HeaderHelper headerHelper() {
    return new HeaderHelper();
  }

  @Bean
  public OpenAPI applicationOpenAPI() {
    return new OpenAPI()
        .info(new Info()
            .title(applicationName)
            .description(applicationName));
  }

}
