package com.mathworks.internal.business.product.archivefile.api.service.support;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ValidatorTest {
  private Validator validator;
  private ParamHelper paramHelper;
  private Map<String, String> params;
  private static final String VAL1 = "value1";
  private static final String VAL2 = "value2";
  private static final String RELEASE_AUDIENCE = "audiences";
  private static final String RELEASE_AVAILABILITY_STATUS = "availabilityStatus";
  private static final String RELATIONSHIP_TYPES = "types";
  private static final String RETURN_DATA = "returnData";

  private static final List<String> PARAMS = Arrays.asList(RELEASE_AUDIENCE,
      RELEASE_AVAILABILITY_STATUS);

  private static final List<String> PARENT_PARAMS = Arrays.asList(RELEASE_AUDIENCE,
      RELEASE_AVAILABILITY_STATUS, RELATIONSHIP_TYPES);
  private static final List<String> RETURN_DATA_VALUES = Arrays.asList("versions");

  @BeforeEach
  void setUp() {
    validator = new Validator();
    paramHelper = new ParamHelper();
  }


  @Test
  void testValidateNoErrors() {
    params = setup(RELEASE_AVAILABILITY_STATUS, ",");
    MultiParams parsedParams = paramHelper.parseParams(params);
    List<String> errors = validator.validate(parsedParams, PARENT_PARAMS);
    assertEquals(0, errors.size());
  }

  @Test
  void testValidateErrors() {
    params = setup(RELATIONSHIP_TYPES, ",");
    MultiParams parsedParams = paramHelper.parseParams(params);
    List<String> errors = validator.validate(parsedParams, PARAMS);
    assertEquals(1, errors.size());
  }

  private Map<String, String> setup(String key, String separator) {
    params = new HashMap<>();
    params.put(key, VAL1 + separator + VAL2);
    return params;
  }
}
