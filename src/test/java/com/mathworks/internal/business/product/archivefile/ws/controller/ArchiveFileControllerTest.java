
package com.mathworks.internal.business.product.archivefile.ws.controller;

import com.mathworks.internal.business.product.archivefile.api.entity.ArchiveFile;
import com.mathworks.internal.business.product.archivefile.api.service.ArchiveFileService;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mathworks.internal.foundation.rest.pagesupport.HeaderHelper;

import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ArchiveFileControllerTest {

  @InjectMocks
  private ArchiveFileController archiveFileController = new ArchiveFileController();
  @Mock
  private ArchiveFileService archiveFileService;
  @Mock
  private HeaderHelper headerHelper;
  @Mock
  private HttpServletResponse response;

  private Map<String, String> params;
  private List<ArchiveFile> archiveFiles;


  @BeforeEach
  void setUp() {
    //acting as a stub for these tests
    params = setupTestParams();
    archiveFiles = new ArrayList<>();
  }

  @Test
  void testGetArchiveFilesByFilter() {
    when(archiveFileService.findArchiveFilesByFilter(params)).thenReturn(archiveFiles);
    when(headerHelper.addPageCounts(response, archiveFiles)).thenReturn(response);
    List<ArchiveFile> result = archiveFileController.getArchiveFilesByFilter(params, response);
    assertSame(archiveFiles, result);
  }

  private Map<String, String> setupTestParams() {
    return new HashMap<>();
  }


}
