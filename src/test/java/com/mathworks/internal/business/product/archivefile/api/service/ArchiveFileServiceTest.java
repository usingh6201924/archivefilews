package com.mathworks.internal.business.product.archivefile.api.service;

import com.mathworks.internal.business.product.archivefile.api.entity.ArchiveFile;
import com.mathworks.internal.business.product.archivefile.api.repository.ArchiveFileRepository;
import com.mathworks.internal.foundation.rest.error.BadRequestException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ArchiveFileServiceTest {
  private static final Integer ID=1;
  private static final String TEST_DATA="test";

  @InjectMocks
  private ArchiveFileService archiveFileService = new ArchiveFileService();
  @Mock
  private ArchiveFileRepository archiveFileRepository;

  private Map<String, String> params;
  private List<ArchiveFile> archiveFiles;


  @BeforeEach
  void setUp() {
    params = new HashMap<>();
    archiveFiles = createArchiveFilesStub();
  }

  @Test
  void testFindArchiveFilesByFilter() {
    List<String> noparam = null;
    params.put("baseCodes", "AA");
    when(archiveFileRepository.findArchiveFilesByFilter(0, noparam,
        0, noparam,
        0, noparam,
        0, noparam,
        1, Arrays.asList("AA"))).thenReturn(archiveFiles);
    List<ArchiveFile> result = archiveFileService.findArchiveFilesByFilter(params);
    assertSame(result, archiveFiles);
    assertResult(result);
  }

  @Test
  void testFindArchiveFilesByFilterBadParams(){
    params.put("bogus", "test");
    assertThrows(BadRequestException.class, () -> {
      archiveFileService.findArchiveFilesByFilter(params);
    });
  }
private List<ArchiveFile> createArchiveFilesStub(){
    List<ArchiveFile>  archiveFiles = new ArrayList<>();
    ArchiveFile archiveFile = new ArchiveFile();
    archiveFile.setId(ID);
    archiveFile.setArchiveFileByteSize(1L);
    archiveFile.setArchiveFileMD5Checksum(TEST_DATA);
    archiveFile.setArchiveFileName(TEST_DATA);
    archiveFile.setArchiveUncompressedFileByteSize(ID);
    archiveFile.setBaseCode(TEST_DATA);
    archiveFile.setCustomerReleaseString(TEST_DATA);
    archiveFile.setFilePathRandomExtension(TEST_DATA);
    archiveFile.setLicenseFileFeatureId(ID);
    archiveFile.setLicenseFileFeatureName(TEST_DATA);
    archiveFile.setArchiveFileTypeId(ID);
    archiveFile.setPlatform(TEST_DATA);
    archiveFile.setPlatformType(TEST_DATA);
    archiveFile.setReleaseAvailabilityDate(TEST_DATA);
    archiveFile.setReleaseName(TEST_DATA);
    archiveFile.setVersion(TEST_DATA);
    archiveFile.setVersionName(TEST_DATA);
    archiveFiles.add(archiveFile);
    return archiveFiles;
}
 public void assertResult(List<ArchiveFile> result){
   result.stream().forEach(archiveFile -> {
     assertTrue(archiveFile.getId() == ID);
     assertTrue(archiveFile.getArchiveFileMD5Checksum().equals(TEST_DATA));
     assertTrue(archiveFile.getArchiveFileName().equals(TEST_DATA));
     assertTrue(archiveFile.getArchiveUncompressedFileByteSize()== ID);
     assertTrue(archiveFile.getBaseCode().equals(TEST_DATA));
     assertTrue(archiveFile.getCustomerReleaseString().equals(TEST_DATA));
     assertTrue(archiveFile.getFilePathRandomExtension().equals(TEST_DATA));
     assertTrue(archiveFile.getPlatform().equals(TEST_DATA));
     assertTrue(archiveFile.getPlatformType().equals(TEST_DATA));
     assertTrue(archiveFile.getLicenseFileFeatureId() == ID);
     assertTrue(archiveFile.getLicenseFileFeatureName().equals(TEST_DATA));
     assertTrue(archiveFile.getArchiveFileTypeId() == ID);
     assertTrue(archiveFile.getReleaseAvailabilityDate().equals(TEST_DATA));
     assertTrue(archiveFile.getReleaseName().equals(TEST_DATA));
     assertTrue(archiveFile.getVersion().equals(TEST_DATA));
     assertTrue(archiveFile.getVersionName().equals(TEST_DATA));
   });
 }
}
