package com.mathworks.internal.business.product.archivefile.api.service.support;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

class MultiParamsTest {

  private MultiParams multiParams;
  private String key;

  @BeforeEach
  void setUp(){
    multiParams = new MultiParams();
  }

  @Test
  void size_keyNotFound(){
    key = "bogus";
    assertEquals(0, multiParams.size(key));
  }

  @Test
  void size(){
    key = "test";
    multiParams.add(key, "value1");
    multiParams.add(key, "value2");
    assertEquals(2, multiParams.size(key));
  }

  @Test
  void getBoolean(){
    key = "test";
    multiParams.add(key, "true");
    assertTrue(multiParams.getBoolean(key));
  }

  @Test
  void getBoolean_valueNotFound(){
    key = "test";
    multiParams.add(key, "bogus");
    assertFalse(multiParams.getBoolean(key));
  }

  @Test
  void getBoolean_keyNotFound(){
    key = "test";
    assertNull(multiParams.getBoolean(key));
  }

}