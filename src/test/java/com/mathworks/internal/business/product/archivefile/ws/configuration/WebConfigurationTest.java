package com.mathworks.internal.business.product.archivefile.ws.configuration;


import com.mathworks.internal.core.healthmonitor.AbstractHealthCheck;
import com.mathworks.internal.core.rest.filter.RestLogFilter;
import com.mathworks.internal.core.springsecurity.filter.SpringSecurityFilter;
import com.mathworks.internal.foundation.namedresource.ResourceHelper;
import com.mathworks.internal.foundation.rest.pagesupport.HeaderHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WebConfigurationTest {

  private WebConfiguration webConfiguration;

  @BeforeEach
  void setUp() {
    webConfiguration = new WebConfiguration();
  }

  @Test
  void springSecurityFilter() {
    SpringSecurityFilter filter = webConfiguration.springSecurityFilter();
    assertTrue(filter.isCallerIdRequired());
  }
  @Test
  void restLogFilter() {
    RestLogFilter restLogFilter = webConfiguration.logFilter();
    assertNotNull(restLogFilter);
  }
  @Test
  void resourceHelper() {
    ResourceHelper resourceHelper = webConfiguration.resourceHelper();
    assertNotNull(resourceHelper);
  }
  @Test
  void headerHelper() {
    HeaderHelper headerHelper = webConfiguration.headerHelper();
    assertNotNull(headerHelper);
  }
  @Test
  void authHealthCheck() {
    AbstractHealthCheck authHealthCheck = webConfiguration.authHealthCheck();
    assertNotNull(authHealthCheck);
  }
}
