
package com.mathworks.internal.business.product.archivefile.ws.configuration;

import com.mathworks.internal.business.product.archivefile.ArchivefilewsApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * Class provided in the test tree to run spring boot app on desktop.  When run from test tree, provided dependencies
 * are included.
 */
public class ArchivefilewsApplicationRunner extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(ArchivefilewsApplication.class, args);
  }

}

