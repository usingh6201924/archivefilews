package com.mathworks.internal.business.product.archivefile.api.service.support;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;


import java.util.List;
import java.util.Map;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParamHelperTest {

  private ParamHelper paramHelper;
  private Map<String, String> params;
  private static final String VAL1 = "value1";
  private static final String VAL2 = "value2";
  private static final String KEY = "key";

  @BeforeEach
  void setUp() {
    paramHelper = new ParamHelper();
    params = setup(",");
  }

  @Test
  void parseParams() {
    MultiParams result = paramHelper.parseParams(params);
    List<String> values = result.get(KEY);
    assertEquals(2, values.size());
    assertEquals(VAL1, values.get(0));
    assertEquals(VAL2, values.get(1));
  }

  @Test
  void parseParams_extraSpaces() {
    params = setup(" , ");
    MultiParams result = paramHelper.parseParams(params);
    List<String> values = result.get(KEY);
    assertEquals(2, values.size());
    assertEquals(VAL1, values.get(0));
    assertEquals(VAL2, values.get(1));
  }

  private Map<String, String> setup(String separator) {
    params = new HashMap<>();
    params.put(KEY, VAL1 + separator + VAL2);
    return params;
  }

}