package com.mathworks.internal.business.product.archivefile.api.configuration;

import com.mathworks.internal.core.healthmonitor.AbstractHealthCheck;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import jakarta.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class DBConfigurationTest {

  private DBConfiguration dbConfiguration;

  @BeforeEach
  void setUp() {
    dbConfiguration = new DBConfiguration();
  }

  @Test
  void dataSource() {
    DataSource dataSource = dbConfiguration.dataSource();
    assertNotNull(dataSource);
  }

  @Test
  void databaseHealthCheck() {
    AbstractHealthCheck databaseHealthCheck = dbConfiguration.databaseHealthCheck();
    assertNotNull(databaseHealthCheck);
  }

  @Test
  void entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean entityManagerFactory = dbConfiguration.entityManagerFactory();
    assertNotNull(entityManagerFactory);
  }

  @Test
  void transactionManager() {
    EntityManagerFactory entityManagerFactory = null;
    JpaTransactionManager transactionManager = dbConfiguration.transactionManager(entityManagerFactory);
    assertNotNull(transactionManager);
  }
}