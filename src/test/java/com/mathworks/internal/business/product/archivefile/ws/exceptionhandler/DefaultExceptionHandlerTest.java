package com.mathworks.internal.business.product.archivefile.ws.exceptionhandler;


import com.mathworks.internal.foundation.rest.common.Message;
import com.mathworks.internal.foundation.rest.error.BadRequestException;
import com.mathworks.internal.foundation.rest.error.ResourceNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class DefaultExceptionHandlerTest {

  private DefaultExceptionHandler defaultExceptionHandler;
  private Message message;
  private static final String TEXT = "test";

  @BeforeEach
  void setUp()  {
    defaultExceptionHandler = new DefaultExceptionHandler();
  }

  @Test
  void createMessage()  {
    Message result = defaultExceptionHandler.createMessage(TEXT);
    assertEquals(TEXT, result.getDetail());
  }

  @Test
  void testHandleNotFoundException(){
    ResourceNotFoundException ex = new ResourceNotFoundException("Dummy");
    Message result = defaultExceptionHandler.handleMissingResourceElement(null, ex);
    assertNotNull(result.getDetail());
    assertNull(result.getRefId());
  }
  @Test
  void testHandleNullPointerException() {
    NullPointerException ex = new NullPointerException("Dummy");
    Message result = defaultExceptionHandler.handleInternalServerError(null, ex);
    assertNotNull(result.getDetail());
  }

  @Test
  void testHandleBadRequestException(){
    BadRequestException ex = new BadRequestException("Dummy",1);
    Message result = defaultExceptionHandler.handleMissingParamError(null, ex);
    assertNotNull(result.getDetail());
  }

}
